<?php

/**
 * @file
 * Filters out nodes outside of the requested range.
 *
 * A views plugin that takes the layar request arguments
 * and inserts a SQL query to filter out the nodes which don't
 * fit inside the layar requests range,
 */

/**
 * Defines a proximity filter for location latitude/longitude.
 */
class layar_handler_filter_proximity extends views_handler_filter {
  /**
   * Overrides views_handler_filter::query()
   *
   * Modifies the query to filter results using the layar GET requests parameters.
   *
   * @see location_search.module
   */
  function query() {
    if (!$this->view->live_preview) { //don't use this filter in live preview mode
      //sanitize fields
      $fields = array();
      $fields['lon'] = isset($_GET['lon']) ? filter_input(INPUT_GET, 'lon', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) : FALSE;
      $fields['lat'] = isset($_GET['lat']) ? filter_input(INPUT_GET, 'lat', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) : FALSE;
      $fields['radius'] = isset($_GET['radius']) ? filter_input(INPUT_GET, 'radius', FILTER_SANITIZE_NUMBER_INT) : FALSE;
      if (!in_array(NULL, $fields) && !in_array(FALSE, $fields)) {
        //earth_distance_sql is a funcion defined in the location_search module (that comes bundled with location)
        $this->query->add_where($this->options['group'], earth_distance_sql((Float)$fields['lon'], (Float)$fields['lat'], 'location') . ' < %f', (Float)$fields['radius']);
      }
    }
  }
}
