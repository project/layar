How to install the layar module:

1. Copy the layar module folder in your sites/all/modules directory
2. On the "http://yoursite/admin/build/modules" turn on the layar module, you may need to download and enable some dependencies first. Layar requires Views, location, gmap and content modules to be installed. The additional modules required are modules that get installed when you install these but are not turned on.

How to use:

1. Create a view
2. Add a display of the type "Feed" and set this displays style plugin to "Layar JSON"
4. Add the "Layar: Location latitude/longitude is within Layars requested Proximity" filter to the display so the view filters out nodes that are not within range of the user.
4. I would reccomend you set the items to display to "0" for unlimited.
5. Then add all of the other filters/relationships .etc you'd like
6. Add all the fields you wish to include in the layar app into the fields box. NOTE: REMEMBER TO SET YOUR LONGITUDE AND LATITUDE FIELDS TO DECIMAL DEGREES.
7. Under the style plugin settings map all the fields to a layar field, you only need to do the required ones as indicated by the star.
8. Create a piece of content of the type "Layar" and highlight all the view displays you want to be included.
9. View the node and use the POI Url displayed on the layar website when creating your layer.





