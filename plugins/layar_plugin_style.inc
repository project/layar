<?php

/**
 * @file
 * A style plugin that creates Layar hotspot objects from the views results.
 *
 * A views style plugin that takes the results from the executed view
 * and creates hotspot objects with the correct structure so they can
 * be encoded into JSON later.
 */

class layar_plugin_style extends views_plugin_style {
  /**
   * Overrides views_plugin_style::render().
   *
   * Renders the views output.
   *
   * @return
   *   The output in the form of a string, a JSON structure of the hotspots if in live preview, otherwise a serialized object to be encoded later.
   */
  function render() {
    //get lat and lon fields
    $fields = array();
    $fields['lat'] = isset($_GET['lat']) ? filter_input(INPUT_GET, 'lat', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) : FALSE;
    $fields['lon'] = isset($_GET['lon']) ? filter_input(INPUT_GET, 'lon', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) : FALSE;
    //check for unsafe data
    if ($this->view->live_preview) {
      drupal_set_message(t('One or more of the layar arguments is not present, This is most likely due JSON being generated within views "Live preview", Because of this the JSON generated below will not take filters based on layar parameters into account and fields which rely on layar parameters will not be calculated e.g. Distance.'));
    }
    elseif (in_array(NULL, $fields) || in_array(FALSE, $fields)) {
      ob_start();
      return array();
    }
    //generate list of hotspots
    $this->render_fields($this->view->result); //render the fields (2 hours to figure this out)
    $hotspots = array();
    if (isset($this->view->style_plugin->rendered_fields)) { //don't foreach on an empty array when no fields are set yet
      foreach ($this->view->style_plugin->rendered_fields as $result) {
        $hotspot = new stdClass();
        $hotspot->type = 0;
        $hotspot->dimension = 1;
        $hotspot->actions = isset($result[$this->options['general']['layar_actions']]) ? explode("\r\n", $result[$this->options['general']['layar_actions']]) : array();
        $hotspot->attribution = isset($result[$this->options['tooltip']['attribution']]) ? (String)$result[$this->options['tooltip']['attribution']] : '';
        if ($this->view->live_preview) {
          $hotspot->distance = 0;
        }
        else {
          $hotspot->distance = ((($fields['lat'] - (Float)$result[$this->options['general']['layar_lat']])*111230)^2 + (($fields['lon'] - (Float)$result[$this->options['general']['layar_lon']])*71700)^2)^0.5;
        }
        $hotspot->id = (Integer)$result[$this->options['general']['layar_id']];
        $hotspot->lat = (Float)$result[$this->options['general']['layar_lat']] * 1000000;
        $hotspot->lon = (Float)$result[$this->options['general']['layar_lon']] * 1000000;
        $hotspot->line2 = isset($result[$this->options['tooltip']['layar_line2']]) ? (String)$result[$this->options['tooltip']['layar_line2']] : '';
        $hotspot->line3 = isset($result[$this->options['tooltip']['layar_line3']]) ? (String)$result[$this->options['tooltip']['layar_line3']] : '';
        $hotspot->line4 = isset($result[$this->options['tooltip']['layar_line4']]) ? (String)$result[$this->options['tooltip']['layar_line4']] : '';
        $hotspot->title = (String)$result[$this->options['tooltip']['layar_title']];
        $hotspot->imageURL = isset($result[$this->options['tooltip']['layar_imageURL']]) ? (String)$result[$this->options['tooltip']['layar_imageURL']] : '';
        $object = new stdClass();
        $object->baseURL = '';
        $object->full = isset($result[$this->options['icon']['layar_full']]) ? (String)$result[$this->options['icon']['layar_full']] : '';
        $object->reduced = isset($result[$this->options['icon']['layar_reduced']]) ? (String)$result[$this->options['icon']['layar_reduced']] : '';
        $object->icon = '';
        $object->size = isset($result[$this->options['icon']['layar_size']]) ? (Float)$result[$this->options['icon']['layar_size']] : 32.5;
        $hotspot->object = $object;
        $hotspots[] = $hotspot;
      }
    }
    //if it's live preview then output JSON
    if ($this->view->live_preview) {
      return json_encode($hotspots);
    }
    else { //if not output a serialized array to a buffer to picked up after the view is finished
      ob_start();
      return serialize($hotspots);
    }
  }
  /**
   * Overrides views_style_plugin::option_definition().
   *
   * Defines all the persistant variables for the views style plugins form.
   *
   * @return
   *   An object defining each variable and it's properties.
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['general']['layar_actions'] = array('default' => '', 'translatable' => FALSE);
    $options['general']['layar_id'] = array('default' => '', 'translatable' => FALSE);
    $options['general']['layar_lat'] = array('default' => '', 'translatable' => FALSE);
    $options['general']['layar_lon'] = array('default' => '', 'translatable' => FALSE);
    $options['tooltip']['layar_title'] = array('default' => '', 'translatable' => FALSE);
    $options['tooltip']['layar_imageURL'] = array('default' => '', 'translatable' => FALSE);
    $options['tooltip']['layar_line2'] = array('default' => '', 'translatable' => FALSE);
    $options['tooltip']['layar_line3'] = array('default' => '', 'translatable' => FALSE);
    $options['tooltip']['layar_line4'] = array('default' => '', 'translatable' => FALSE);
    $options['tooltip']['layar_attribution'] = array('default' => '', 'translatable' => FALSE);
    $options['icon']['layar_full'] = array('default' => '', 'translatable' => FALSE);
    $options['icon']['layar_reduced'] = array('default' => '', 'translatable' => FALSE);
    $options['icon']['layar_size'] = array('default' => '32.5', 'translatable' => FALSE);
    return $options;
  }
  /**
   * Overrides views_style_plugin::options_form().
   *
   * Defines the form object for the style plugin.
   */
  function options_form(&$form, &$form_state) {
    //get the default and current displays definitions
    $current_display = $this->view->display[$this->view->current_display]->display_options;
    $default_display = $this->view->display['default']->display_options;
    //find out which definition contains the fields (depends whether the display is overriding defaults or not)
    if (isset($current_display['defaults']['fields'])) {
      $field_definitions = $current_display['fields'];
    }
    else {
      $field_definitions = $default_display['fields'];
    }
    //build a nice array to include in the select boxes
    $fields = array();
    if (isset($field_definitions)) {
      $fields['-'] = '-'; //add a default option
      foreach ($field_definitions as $field_definition_id => $field_definition_value) {
        $fields[$field_definition_id] = $field_definition_value['table'] . ': ' . $field_definition_value['label'];
      }
    }
    else {
      $form['nofields'] = array(
        '#value' => t('Some fields must be defined before you can map them to the layar JSON fields.'),
      );
      return;
    }
    //make the form elements
    $form['general'] = array(
      '#type' => 'fieldset',
      '#title' => t('General Information'),
      '#collapsible' => TRUE,
    );
    $form['tooltip'] = array(
      '#type' => 'fieldset',
      '#title' => t('Tooltip Information'),
      '#collapsible' => TRUE,
    );
    $form['icon'] = array(
      '#type' => 'fieldset',
      '#title' => t('Icon Information'),
      '#collapsible' => TRUE,
    );
    $form['general']['layar_actions'] = array(
      '#type' => 'select',
      '#title' => t('Layar Actions'),
      '#default_value' => $this->options['general']['layar_actions'],
      '#description' => t('A set of URL\'s to apply to the POI. Set this to a text field which has a separate action URL on each line.'),
      '#options' => $fields,
    );
    $form['general']['layar_id'] = array(
      '#type' => 'select',
      '#title' => t('Layar Unique ID'),
      '#default_value' => $this->options['general']['layar_id'],
      '#description' => t('A unique id for the POI, set this to \'Node: Nid\' unless your doing something weird.'),
      '#options' => $fields,
      '#required' => TRUE,
    );
    $form['general']['layar_lat'] = array(
      '#type' => 'select',
      '#title' => t('Layar Latitude'),
      '#default_value' => $this->options['general']['layar_lat'],
      '#description' => t('The latitude of the POI. Note: Don\'t multiply by 1000000.'),
      '#options' => $fields,
      '#required' => TRUE,
    );
    $form['general']['layar_lon'] = array(
      '#type' => 'select',
      '#title' => t('Layar Longitude'),
      '#default_value' => $this->options['general']['layar_lon'],
      '#description' => t('The longitude of the POI. Note: Don\'t multiply by 1000000.'),
      '#options' => $fields,
      '#required' => TRUE,
    );
    $form['tooltip']['layar_title'] = array(
      '#type' => 'select',
      '#title' => t('Layar Title'),
      '#default_value' => $this->options['tooltip']['layar_title'],
      '#description' => t('The title of the POI.'),
      '#options' => $fields,
      '#required' => TRUE,
    );
    $form['tooltip']['layar_imageURL'] = array(
      '#type' => 'select',
      '#title' => t('Layar Image URL'),
      '#default_value' => $this->options['tooltip']['layar_imageURL'],
      '#description' => t('The icon to display in the tooltip box.'),
      '#options' => $fields,
    );
    $form['tooltip']['layar_line2'] = array(
      '#type' => 'select',
      '#title' => t('Layar Line2'),
      '#default_value' => $this->options['tooltip']['layar_line2'],
      '#description' => t('Line 2 in the tooltip box.'),
      '#options' => $fields,
    );
    $form['tooltip']['layar_line3'] = array(
      '#type' => 'select',
      '#title' => t('Layar Line3'),
      '#default_value' => $this->options['tooltip']['layar_line3'],
      '#description' => t('Line 3 in the tooltip box.'),
      '#options' => $fields,
    );
    $form['tooltip']['layar_line4'] = array(
      '#type' => 'select',
      '#title' => t('Layar Line4'),
      '#default_value' => $this->options['tooltip']['layar_line4'],
      '#description' => t('Line 4 in the tooltip box.'),
      '#options' => $fields,
    );
    $form['tooltip']['layar_attribution'] = array(
      '#type' => 'select',
      '#title' => t('Layar Attribution'),
      '#default_value' => $this->options['tooltip']['layar_attribution'],
      '#description' => t('The attribution of the tooltip box (in italic at the bottom).'),
      '#options' => $fields,
    );
    $form['icon']['layar_full'] = array(
      '#type' => 'select',
      '#title' => t('Layar Full Icon URL'),
      '#default_value' => $this->options['icon']['layar_full'],
      '#description' => t('The icon when it is very close to the user.'),
      '#options' => $fields,
    );
    $form['icon']['layar_reduced'] = array(
      '#type' => 'select',
      '#title' => t('Layar Reduced Icon URL'),
      '#default_value' => $this->options['icon']['layar_reduced'],
      '#description' => t('The icon when it is far away, you can use the same URL as the full (this is here so you can provide a smaller image that hasn\'t been damaged by resizing). If not set then layar will use the default layar icon.'),
      '#options' => $fields,
    );
    $form['icon']['layar_size'] = array(
      '#type' => 'textfield',
      '#title' => t('Layar Object Size'),
      '#default_value' => $this->options['icon']['layar_size'],
      '#description' => t('The size of the object in square meters.'),
    );
  }
}


