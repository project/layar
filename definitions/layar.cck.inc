<?php

/**
 * @file
 * Defines the Layar content type.
 *
 * An export from the content copy module of the layar content type
 * modified slightly so the array it pushes information into is global.
 */

global $_layar_content_type;
$_layar_content_type['type']  = array(
  'name' => 'Layar',
  'type' => 'layar',
  'description' => 'Add a layer with default icon sets and views that define the layars properties.',
  'title_label' => 'Title',
  'body_label' => '',
  'min_word_count' => '0',
  'help' => '',
  'node_options' =>
  array(
    'status' => TRUE,
    'promote' => TRUE,
    'sticky' => FALSE,
    'revision' => FALSE,
  ),
  'upload' => 1,
  'old_type' => 'layar',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'comment' => '2',
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
);
$_layar_content_type['fields']  = array(
  0 =>
  array(
    'label' => 'Layar Display View',
    'field_name' => 'field_layar_view',
    'type' => 'text',
    'widget_type' => 'optionwidgets_select',
    'change' => 'Change basic information',
    'weight' => '2',
    'description' => 'A view for defining what fields map to the fields displayed in layar.',
    'default_value' =>
    array(
      0 =>
      array(
        'value' => '',
      ),
    ),
    'default_value_php' => '',
    'default_value_widget' => NULL,
    'group' => FALSE,
    'required' => 1,
    'multiple' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '$options = array();
$views = views_get_all_views(); //get an array of all the views
foreach($views as $view) { //loop through each view making a list of displays for each
  $displays = array();
  $display_names = array_keys($view->display); //get an array of the display id\'s
  foreach($display_names as $display_name) { //loop through each display building an entry for each
    $displays[$view->name . \'-\' . $display_name] = $view->display[$display_name]->display_title; //set the id as "viewsid-displayid", get the human name from elsewhere in the view object
  }
  $options[$view->name] = $displays; //set the list of displays to appear as children of the view name
}
return $options;',
    'op' => 'Save field settings',
    'module' => 'text',
    'widget_module' => 'optionwidgets',
    'columns' =>
    array(
      'value' =>
      array(
        'type' => 'text',
        'size' => 'big',
        'not null' => FALSE,
        'sortable' => TRUE,
        'views' => TRUE,
      ),
    ),
    'display_settings' =>
    array(
      'weight' => '-4',
      'parent' => '',
      'label' =>
      array(
        'format' => 'above',
      ),
      'teaser' =>
      array(
        'format' => 'plain',
        'exclude' => 0,
      ),
      'full' =>
      array(
        'format' => 'plain',
        'exclude' => 0,
      ),
      4 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      2 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
      3 =>
      array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$_layar_content_type['extra']  = array(
  'title' => '1',
  'revision_information' => '6',
  'author' => '5',
  'options' => '7',
  'comment_settings' => '8',
  'menu' => '4',
  'path' => '10',
  'attachments' => '9',
);

