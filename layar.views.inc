<?php

/**
 * @file
 * Defines the layar views styles and filters
 *
 * Defines the layar proximity filter and the layar style plugin
 * to the views module.
 */

/**
 * Implementation of hook_views_plugins().
 */
function layar_views_plugins() {
  return array(
    'module' => 'layar',
    'style' => array(
      'layar-json' => array(
        'title' => t('Layar JSON'),
        'help' => t('Builds the JSON response to layar.'),
        'handler' => 'layar_plugin_style',
        'theme' => 'layar-json',
        'path' => drupal_get_path('module', 'layar') . '/plugins',
        'uses row plugin' => FALSE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'feed',
      ),
    )
  );
}

/**
 * Implementation of hook_views_handlers().
 */
function layar_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'layar') . '/handlers',
    ),
    'handlers' => array(
      'layar_handler_filter_proximity' => array(
        'parent' => 'views_handler_filter',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 */
function layar_views_data() {
  $data['layar']['table']['group'] = t('Layar');
  $data['layar']['table']['join'] = array(
    '#global' => array(),
  );
  $data['layar']['latlon'] = array(
    'title' => t('Location latitude/longitude is within Layars requested Proximity'),
    'help' => t('Performs a proximity search between layars lat/lon arguments and the location tables lat/lon to the layar arguments radius field.'),
    'filter' => array(
      'handler' => 'layar_handler_filter_proximity',
    ),
  );
  return $data;
}

